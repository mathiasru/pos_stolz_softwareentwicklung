package itkolleg.com;

class MyLinkedList<T>{

    public Element<T> head;

    /**
     * Hinzufügen eines Elementes am Ende der Liste
     * @param data übergebenes Element
     */
    public void add(T data){
        Element<T> newElement = new Element<>(data);

        if(head == null){
            head = newElement;
        }
        else {
            Element<T> lastElement = lastElement();
            lastElement.nextElement = newElement;
        }
    }

    /**
     * Gibt das letzte Element der liste zurück.
     * @return letztes Element der Liste
     */
    private Element<T> lastElement() {
        Element<T> lastElement = head;

        while(lastElement.nextElement != null){
            lastElement = lastElement.nextElement;
        }
        return lastElement;
    }

    /**
     * Hinzufügen eines Elementes an einer bestimmten Index-Position.
     * Wenn der Index außerhalb des Bereiches liegt, wird das Element einfach am Ende angefügt.
     * @param data übergebenes Element
     * @param index übergebener Index
     */
    public void addAtIndex(T data, int index){
        Element<T> newElement = new Element<>(data);
        Element<T> lastElement = this.head;

        if (index == 0){
            this.head = newElement;
            this.head.nextElement = lastElement;
            return;
        }

        if (index < size() && index > 0 ){
            int count = 0;
            while(lastElement.nextElement != null){
                if (count == index -1 ){
                    Element<T> elementbefore = lastElement;
                    Element<T> elementafter = lastElement.nextElement;
                    lastElement.nextElement = newElement;
                    newElement.nextElement = elementafter;
                    break;
                }
                lastElement = lastElement.nextElement;
                count++;
            }
        } else {
            lastElement().nextElement = newElement;
        }
    }

    /**
     * Gibt die Anzahl an Elementen zurück.
     * @return Anzahl der Elemente vom Typ int.
     */
    public int size(){
        int size = 0;
        if (head != null){
            Element<T> lastElement = head;
            size++;
            while(lastElement.nextElement != null){
                lastElement = lastElement.nextElement;
                size++;
            }
        }
        return size;
    }

    @Override
    public String toString(){
        String returnString = "";

        Element<T> currentElement = head;

        while(currentElement != null){
            returnString += currentElement.data;
            returnString += "\n";
            currentElement = currentElement.nextElement;
        }

        return returnString;
    }

}