package itkolleg.com;

class Element<D> {
    public D data;
    public Element<D> nextElement;

    public Element(D data) {
        this.data = data;
    }
}