package itkolleg.com;

public class main {

    public static void main(String[] args) {
        MyLinkedList<String> myList = new MyLinkedList<>();
        myList.add("Erstes Element");
        myList.add("Zweites Element");
        myList.add("Drittes Element");
        System.out.println("Size: " + myList.size());
        System.out.println(myList);

        myList.addAtIndex("neues Element", 0);
        System.out.println("Size: " + myList.size());
        System.out.println(myList);
    }
}