package com.company;

public class Main {

    public static void main(String[] args) {
        Thread t1 = new Thread(new DateCommand()); //Erzeugt einen neuen Thread mit einem Runnable, das den nebenläufig auszuführenden Programmcode vorgib
        t1.start(); // Ein neuer Thread – neben dem die Methode aufrufenden Thread – wird gestartet

        Thread t2 = new Thread(new CounterCommand());
        t2.start();

        Thread t = new DateThread();
        t.start();
        new DateThread().start();      // alternative Schreibweise ohne Objektreferenz
    }
}
