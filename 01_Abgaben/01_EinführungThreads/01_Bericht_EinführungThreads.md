# Nebenläufigkeit

- [Nebenläufigkeit](#nebenläufigkeit)
- [Einführung in die nebenläufige Programmierung](#einführung-in-die-nebenläufige-programmierung)
  - [Java-Bibliothek für nebenläufige Programme](#java-bibliothek-für-nebenläufige-programme)
  - [Threads erzeugen](#threads-erzeugen)
  - [Klasse Thread erweitern](#klasse-thread-erweitern)
- [Literaturverzeichnis](#literaturverzeichnis)

# Einführung in die nebenläufige Programmierung

- Nebenläufige Programme werden in Java durch Threads realisiert und vom Betriebssystem(Scheduler) getimt.
- Es kann nur durch mehrere Prozessorkerne ein Nebenläufigkeit erzeugt werden, ansonsten wird es nur schnell hintereinander ausgeführt (vorgegaukelt).
- Ein Prozess setzt sich aus Programmcode und Daten zusammen und besitzt einen eigenen Adressraum.
- Ein Prozess kann aus mehreren Threads bestehen.
- Damit Prozesse untereinander Daten austauschen können, wird ein besonderer Speicherbereich als Shared Memory markiert.
- Durch nebenläufige Modellierung eine Leistungssteigerung möglich ist, da die bei langsamen Operationen anfallenden Wartezeiten genutzt werden können. (muss gut geplant werden)

| Reccourse | Belastung |
| --- | --- |
| Hauptspeicherzugriffe | Prozessor |
| Dateioperationen | Festplatte |
| Datenbankzugriffe | Server, Nertzwerkverbindungen |

## Java-Bibliothek für nebenläufige Programme

- Thread: Jeder laufende Thread ist ein Exemplar dieser Klasse.
- Runnable: Beschreibt den Programmcode, den die JVM nebenläufig ausführen soll.
- Lock: Dient dem Markieren von kritischen Abschnitten, in denen sich nur ein Thread befinden darf.
- Condition: Threads können auf die Benachrichtigung anderer Threads warten.

## Threads erzeugen

- über das Interface Runnable und dessen einzige Methode `run()` können nebenläufige Operationen umgesetzt werden.
- Trotz anderer Reihenfolge wird zuerst der schnellere Thread mit der Zahlenausgabe durchgeführt.

Thread 1 

```java
public class DateCommand implements Runnable{
    @Override public void run(){
        for (int i = 0; i < 20; i++) {
            System.out.println(new java.util.Date());
        }
    }
}
```

Thread 2

```java
public class CounterCommand implements Runnable{
    @Override public void run(){
        for (int i = 0; i < 20; i++) {
            System.out.println(i);
        }
    }
}
```

Damit der Programmcode neben der eigentlichen Applikation läuft, müssen wir ein Thread-Objekt mit dem Runnable verbinden und dann den Thread explizit starten.

```java
public class Main {

    public static void main(String[] args) {
			Thread t1 = new Thread(new DateCommand()); //Erzeugt einen neuen Thread mit einem Runnable, das den nebenläufig auszuführenden Programmcode vorgib
	    t1.start(); // Ein neuer Thread – neben dem die Methode aufrufenden Thread – wird gestartet

	    Thread t2 = new Thread(new CounterCommand());
	    t2.start(); 
    }
}
```

Wenn ein Objekt  innerhalb einer Vererbungshirarchie erzeugt wird, startet ein Thread immer beim Konstruktoraufruf der Oberklasse.

## Klasse Thread erweitern

Erbt von Thread 

```java
public class DateThread extends Thread {

    public DateThread() {
        start(); //damit die Methode nicht extra aufgerufen werden muss
    }

    @Override
    public void run() {
        for (int i = 0; i < 20; i++)
            System.out.println((new java.util.Date())+" Thread1");
    }
}
```

Aufruf

```java
Thread t = new DateThread();
new DateThread(); // alternative Schreibweise ohne Objektreferenz
```

Wenn beim Aufrufen `run()` anstatt von `start()` aufgerufen wird, wird das Programm ganz normal sequenziell ausgeführt und ist somit nicht nebenläufig!!!!

# Literaturverzeichnis

- [https://openbook.rheinwerk-verlag.de/javainsel/15_001.html](https://openbook.rheinwerk-verlag.de/javainsel/15_001.html)